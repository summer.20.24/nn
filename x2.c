// A single Neural network: A perceptron, by Menilek.A
// Like a note or smth  
//
// github.com/Neropox

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
//#include <time.h>

// data the model will be training on
float train[][2] = 
{
  // the expected input is the stuff on the right, what we want the model to predict
  {0,0},
  {1,2},
  {2,4},
  {3,6},
  {4,8},
  // basically multiplication of numbers by 2
};

#define train_count sizeof(train)/sizeof(train[0])

float rand_float(void)
{
  return (float) rand() / (float) RAND_MAX;
}

// the cost function that determines how well the model is performing
// search it up.
float cost(float w, float b) 
{
  float result = 0.0f;

  for (size_t i = 0; i < train_count; ++i) {
    // y = w*x, w is the parameter of the model. I think this is the expression for a single neuron. Also called a 'Perceptron'
    float x = train[i][0];
    float y = w*x + b;
    float error = y - train[i][1];
    result += error*error;
  }

  result /= train_count;
  return result;
}


int main()
{
  //srand(time(0));
  srand(69);
  float w = rand_float()*10.0f;
  float b = rand_float()*5.0f;

  float eps = 1e-3;
  float rate = 1e-3;

  printf("%f\n", cost(w, b));
  for (size_t i = 0; i < 100*100; ++i) {

    float dw = (cost(w + eps, b) - cost(w, b))/eps; 
    float db = (cost(w, eps + b) - cost(w, b))/eps; // derivative of the cost function written in the form: f'(x) = [ f(x + h) - f(x) ] / h.
                                                // check out 'Finite difference' on wikipedia. It is used to approzimate derivatives
                                                // also check 'Learning rate in ML'
                                                // we need the derivative to go the opposite way of the direction in which the function is growing
                                                // b is something called 'Bias'. No idea what the use of the thing is but it apparently helps idk
    w -= dw*rate;
    b -= db*rate;
    printf("cost = %f, w =%f, b=%f\n", cost(w, b), w, b);
  }

  printf("''''''''''''''''''''''''''''''''''''''''\n");
  printf("w = %f, b = %f", w, b);


  return 0;
}
